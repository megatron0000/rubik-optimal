{
  "targets": [
    {
      "target_name": "solve_cube",
      "sources": [ "nodebinding.cpp"],
      "cflags": ["-Wall", "-std=c++11", "-O3", "-Winline"],
      "xcode_settings": {
        "OTHER_CFLAGS": ["-Wall", "-std=c++11", "-O3", "-Winline"]
      }
    }
  ]
}
