const cube_solver = require('./build/Release/solve_cube.node')


const URF = 0
const ULF = 1
const DRF = 2
const DLF = 3
const URB = 4
const ULB = 5
const DRB = 6
const DLB = 7

const UF = 0
const DF = 1
const UB = 2
const DB = 3
const UL = 4
const UR = 5
const DL = 6
const DR = 7
const LF = 8
const RB = 9
const LB = 10
const RF = 11


const EdgeCubieName = ["UF", "DF", "UB", "DB", "UL", "UR",
    "DL", "DR", "LF", "RB", "LB", "RF"]

const CornerCubieName = ["URF", "ULF", "DRF", "DLF",
    "URB", "ULB", "DRB", "DLB"]

let CornerCubieIndex = [];
for (let i = 0; i < 8; i++) {
    CornerCubieIndex[CornerCubieName[i]] = i
}

let EdgeCubieIndex = [];
for (let i = 0; i < 12; i++) {
    EdgeCubieIndex[EdgeCubieName[i]] = i
}

const I = 0
const G = 1
const G2 = 2
const Re = 3
const GRe = 4
const G2Re = 5

const faceindex = {
    "U": 0,
    "R": 1,
    "F": 2,
    "D": 3,
    "L": 4,
    "B": 5
}

function has(array, element) {
    return array.find(el => el == element);
}

function string2cube(cubestring) {
    let colors = cubestring.split('')
    let cube = { corners: [], edges: [], centers: [] }
    cube.corners[ULB] = [colors[0], colors[9 + 0], colors[4 * 9 + 2]]
    cube.corners[URB] = [colors[2], colors[4 * 9], colors[3 * 9 + 2]]
    cube.corners[ULF] = [colors[6], colors[2 * 9 + 0], colors[9 + 2]]
    cube.corners[URF] = [colors[8], colors[3 * 9], colors[2 * 9 + 2]]
    cube.corners[DLB] = [colors[5 * 9 + 6], colors[4 * 9 + 8], colors[9 + 6]]
    cube.corners[DRB] = [colors[5 * 9 + 8], colors[3 * 9 + 8], colors[4 * 9 + 6]]
    cube.corners[DLF] = [colors[5 * 9], colors[9 + 8], colors[2 * 9 + 6]]
    cube.corners[DRF] = [colors[5 * 9 + 2], colors[2 * 9 + 8], colors[3 * 9 + 6]]
    cube.edges[UB] = [colors[1], colors[4 * 9 + 1]]
    cube.edges[UR] = [colors[5], colors[3 * 9 + 1]]
    cube.edges[UF] = [colors[7], colors[2 * 9 + 1]]
    cube.edges[UL] = [colors[3], colors[9 + 1]]
    cube.edges[LF] = [colors[2 * 9 + 3], colors[9 + 5]]
    cube.edges[LB] = [colors[4 * 9 + 5], colors[9 + 3]]
    cube.edges[RB] = [colors[4 * 9 + 3], colors[3 * 9 + 5]]
    cube.edges[RF] = [colors[2 * 9 + 5], colors[3 * 9 + 3]]
    cube.edges[DB] = [colors[5 * 9 + 7], colors[4 * 9 + 7]]
    cube.edges[DR] = [colors[5 * 9 + 5], colors[3 * 9 + 7]]
    cube.edges[DF] = [colors[5 * 9 + 1], colors[2 * 9 + 7]]
    cube.edges[DL] = [colors[5 * 9 + 3], colors[9 + 7]]
    cube.centers["U"] = colors[4]
    cube.centers["D"] = colors[5 * 9 + 4]
    cube.centers["R"] = colors[3 * 9 + 4]
    cube.centers["L"] = colors[9 + 4]
    cube.centers["F"] = colors[2 * 9 + 4]
    cube.centers["B"] = colors[4 * 9 + 4]
    colors = null
    let res = { corners: [], edges: [] }
    for (let i = 0; i < 8; i++) {
        res.corners[i] = {}
        res.corners[i].replaced_by = CornerCubieIndex[(has(cube.corners[i], cube.centers["U"]) ? "U" : "D") + (has(cube.corners[i], cube.centers["R"]) ? "R" : "L") + (has(cube.corners[i], cube.centers["F"]) ? "F" : "B")]
        res.corners[i].orientation = cube.corners[i].indexOf(cube.centers[CornerCubieName[res.corners[i].replaced_by][0]])
    }
    for (let i = 0; i < 12; i++) {
        res.edges[i] = {};
        replaced_by = ""
        for (let face of ["U", "D", "L", "R", "F", "B"]) {
            if (has(cube.edges[i], cube.centers[face])) {
                replaced_by += face
            }
        }
        res.edges[i].replaced_by = EdgeCubieIndex[replaced_by]
        let anchor
        if (replaced_by[0] == "L" || replaced_by[0] == "R") {
            anchor = cube.centers[replaced_by[1]]
        } else {
            anchor = cube.centers[replaced_by[0]]
        }
        res.edges[i].orientation = cube.edges[i][0] == anchor ? 0 : 1
    }
    return res;
}

// cube_solver.solve_cube(string2cube("bbbbbbbbbooooooooowwwwwwwwwrrrrrrrrryyyyyyyyyggggggggg"), function (solution) {
//     console.log(solution)
// })

console.log(string2cube("bbbbbbbbbwwwoooooorrrwwwwwwyyyrrrrrroooyyyyyyggggggggg"))
cube_solver.solve_cube(string2cube("bbbbbbbbbwwwoooooorrrwwwwwwyyyrrrrrroooyyyyyyggggggggg"), function (solution) {
    console.log(solution)
})